import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

function readTextFile(file, callback) {
  var rawFile = new XMLHttpRequest();
  rawFile.overrideMimeType("application/json");
  rawFile.open("GET", file, true);
  rawFile.onreadystatechange = function() {
      if (rawFile.readyState === 4 && rawFile.status === 200) {
          callback(rawFile.responseText);
      }
  }
  rawFile.send(null);
}

readTextFile("jest.json", function(text){
  var data = JSON.parse(text);
  console.log(data);
  ReactDOM.render(<App data={data} />, document.getElementById('root'));
});
