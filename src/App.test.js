import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

describe('App component', () => {
  test('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });
});

describe('Owen', () => {
  test('is 29', () => {
    expect(true).toBeTruthy();
  });

  test('is a man', () => {
    expect(true).toBeTruthy();
  });

  describe('nested describe', () => {
    test('test 1', () => {
      expect(true).toBeTruthy();
    });
  
    test('test 2', () => {
      expect(true).toBeTruthy();
    });
  });
});